package com.powellapps.mymigo;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.powellapps.mymigo.adapter.AdapterMeusAnimaisSpinner;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.model.Oferta;
import com.powellapps.mymigo.model.Singleton;
import com.powellapps.mymigo.model.TiposDeOfertas;
import com.powellapps.mymigo.utils.ConstantsUtils;
import com.powellapps.mymigo.utils.DinheiroUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;

public class NovaOfertaActivity extends AppCompatActivity {

    private EditText editTextTitulo;
    private EditText editTextDescricao;
    private EditText editTextValor;
    private Spinner spinnerTipo;
    private Spinner spinnerAnimal;
    private ArrayList<Animal> animais;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_oferta);
        atribuiViews();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Buscando meus animais...");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ConstantsUtils.REF_ANIMAIS).equalTo(ConstantsUtils.ID_PROPRIETARIO, Singleton.getInstance().getId()).getRef();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                animais = new ArrayList<>();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    final Animal animal = postSnapshot.getValue(Animal.class);
                    animais.add(animal);
                }
                atualiza(animais);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                animais = new ArrayList<>();
                atualiza(animais);
                progressDialog.dismiss();
            }
        });
    }

    private void atualiza(ArrayList<Animal> animais) {
        ArrayAdapter<Animal> adapter =
                new ArrayAdapter<Animal>(this, android.R.layout.simple_spinner_dropdown_item, animais);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAnimal.setAdapter(adapter);
    }

    private void atribuiViews(){
        editTextDescricao = (EditText) findViewById(R.id.editTextDescricao);
        editTextTitulo = (EditText) findViewById(R.id.editTextTitulo);
        editTextValor = (EditText) findViewById(R.id.editTextValor);
        spinnerAnimal = (Spinner) findViewById(R.id.spinnerAnimal);
        spinnerTipo = (Spinner) findViewById(R.id.spinnerTiposDeOfertas);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_nova_oferta, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_salvar:
                capturaDados();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void capturaDados() {
        Oferta oferta = new Oferta();
        oferta.setNovo(true);
        oferta.setTitulo(editTextTitulo.getText().toString());
        oferta.setAnimal((Animal) spinnerAnimal.getSelectedItem());
        oferta.setDescricao(editTextDescricao.getText().toString());
        oferta.setTipo((String) spinnerTipo.getSelectedItem());
        oferta.setValor(Double.valueOf(editTextValor.getText().toString()));
        oferta.setData(Calendar.getInstance().getTimeInMillis());
        oferta.setStatus(TiposDeOfertas.ATIVO.name());

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ConstantsUtils.REF_OFERTAS).push();
        myRef.setValue(oferta);
        finish();
    }
}

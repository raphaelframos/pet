package com.powellapps.mymigo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.powellapps.mymigo.R;

/**
 * Created by raphaelramos on 10/06/17.
 */

public class AdapterImagens extends RecyclerView.Adapter<AdapterImagens.ImagemViewHolder>{

    private ArrayList<String> imagens;
    private Context context;

    public AdapterImagens(Context context, ArrayList<String> imagens) {
        this.context = context;
        this.imagens = imagens;
    }

    @Override
    public ImagemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_imagens, parent, false);
        return new ImagemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImagemViewHolder holder, int position) {
        String imagem = imagens.get(position);
        Picasso.with(context).load(imagem).into(holder.imageViewFoto);
    }

    @Override
    public int getItemCount() {
        return imagens.size();
    }

    public class ImagemViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageViewFoto;

        public ImagemViewHolder(View itemView) {
            super(itemView);
            imageViewFoto = (ImageView) itemView.findViewById(R.id.imageViewAnimal);
        }
    }
}

package com.powellapps.mymigo.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.powellapps.mymigo.R;
import com.powellapps.mymigo.model.Vacina;
import com.powellapps.mymigo.utils.DateUtils;

import java.util.ArrayList;

/**
 * Created by raphaelramos on 17/06/17.
 */

public class AdapterVacina extends RecyclerView.Adapter<AdapterVacina.VacinaViewHolder> {

    private ArrayList<Vacina> vacinas;
    private Context context;

    public AdapterVacina(FragmentActivity context, ArrayList<Vacina> vacinas) {
        this.vacinas = vacinas;
        this.context = context;
    }


    @Override
    public VacinaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_vacina, parent, false);
        return new VacinaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VacinaViewHolder holder, int position) {
        Vacina vacina = vacinas.get(position);

        holder.textViewNome.setText(vacina.getNome());
        holder.textViewData.setText(DateUtils.format(vacina.getData()));
        if(vacina.getVacinado()){
            holder.textViewVacinado.setText("Vacinado");
        }
    }

    @Override
    public int getItemCount() {
        return vacinas.size();
    }

    public void adiciona(Vacina vacina) {
        if(vacinas == null){
            vacinas = new ArrayList<>();
        }
        this.vacinas.add(vacina);
        notifyDataSetChanged();
    }

    public class VacinaViewHolder extends RecyclerView.ViewHolder{

        private TextView textViewNome;
        private TextView textViewData;
        private TextView textViewVacinado;

        public VacinaViewHolder(View itemView) {
            super(itemView);

            textViewData = (TextView) itemView.findViewById(R.id.textViewDataVacina);
            textViewNome = (TextView) itemView.findViewById(R.id.textViewNomeVacina);
            textViewVacinado = (TextView) itemView.findViewById(R.id.textViewVacinado);
        }
    }
}

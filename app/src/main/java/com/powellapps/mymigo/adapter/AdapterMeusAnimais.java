package com.powellapps.mymigo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.powellapps.mymigo.AnimalActivity;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.utils.ConstantsUtils;
import com.powellapps.mymigo.utils.DateUtils;
import com.powellapps.mymigo.utils.PicassoUtils;
import com.powellapps.mymigo.R;
import java.util.ArrayList;

/**
 * Created by raphaelramos on 09/06/17.
 */

public class AdapterMeusAnimais extends RecyclerView.Adapter<AdapterMeusAnimais.AnimalViewHolder>{

    ArrayList<Animal> animais;
    private Context context;

    public AdapterMeusAnimais(Context context, ArrayList<Animal> animais) {
        this.animais = animais;
        this.context = context;
    }

    @Override
    public AnimalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_meus_animais, parent,false);
        AnimalViewHolder animalViewHolder = new AnimalViewHolder(view);
        return animalViewHolder;
    }

    @Override
    public void onBindViewHolder(AnimalViewHolder holder, int position) {
        final Animal animal = animais.get(position);
        holder.textViewNome.setText(animal.getNome());
        holder.textViewRaca.setText(animal.getRaca());
        holder.textViewIdade.setText(DateUtils.getIdade(animal.getNascimento()) + " " + context.getString(R.string.meses));
        PicassoUtils.padrao(context, animal.getImagem(), holder.imageViewFoto);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(context, AnimalActivity.class);
                it.putExtra(ConstantsUtils.ANIMAL, animal);
                context.startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        return animais.size();
    }

    public void atualiza(ArrayList<Animal> animais) {
        this.animais = animais;
        notifyDataSetChanged();
    }

    public class AnimalViewHolder extends RecyclerView.ViewHolder{

        private TextView textViewNome;
        private TextView textViewRaca;
        private TextView textViewIdade;
        private ImageView imageViewFoto;

        public AnimalViewHolder(View itemView) {
            super(itemView);
            textViewNome = (TextView) itemView.findViewById(R.id.textViewNome);
            textViewRaca = (TextView) itemView.findViewById(R.id.textViewRaca);
            textViewIdade = (TextView) itemView.findViewById(R.id.textViewIdade);
            imageViewFoto = (ImageView) itemView.findViewById(R.id.imageViewFoto);
        }
    }
}

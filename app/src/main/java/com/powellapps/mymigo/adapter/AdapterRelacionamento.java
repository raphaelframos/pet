package com.powellapps.mymigo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.powellapps.mymigo.AnimalActivity;
import com.powellapps.mymigo.R;
import com.powellapps.mymigo.model.Oferta;
import com.powellapps.mymigo.utils.CircleTransform;
import com.powellapps.mymigo.utils.ConstantsUtils;

/**
 * Created by raphaelramos on 10/06/17.
 */

public class AdapterRelacionamento extends RecyclerView.Adapter<AdapterRelacionamento.RelacionamentoViewHolder> {

    private ArrayList<Oferta> ofertas;
    private Context context;

    public AdapterRelacionamento(ArrayList<Oferta> ofertas, Context context) {
        this.ofertas = ofertas;
        this.context = context;
    }

    @Override
    public RelacionamentoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_relacionamento, parent, false);
        return new RelacionamentoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RelacionamentoViewHolder holder, int position) {
        final Oferta oferta = ofertas.get(position);
        holder.textViewNome.setText(oferta.getAnimal().getNome());
        holder.textViewRaca.setText(oferta.getAnimal().getRaca());
        holder.textViewTipo.setText(oferta.getAnimal().getTipo());
        holder.textViewSexo.setText(oferta.getAnimal().getSexo());
        if(oferta.getAnimal().getPedigree()){
            holder.textViewPedigree.setText("com Pedigree");
            holder.textViewPedigree.setVisibility(View.VISIBLE);
        }else{
            holder.textViewPedigree.setVisibility(View.GONE);
        }

        holder.textViewProcura.setText(oferta.getProcura());
        try{
            Picasso.with(context).load(oferta.getAnimal().getImagens().get(0))
                    .resize(200, 200)
                    .transform(CircleTransform.getArredondado()).into(holder.imageViewFoto);
        }catch (Exception e){
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(context, AnimalActivity.class);
                it.putExtra(ConstantsUtils.ANIMAL, oferta.getAnimal());
                context.startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ofertas.size();
    }

    public void atualiza(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
        notifyDataSetChanged();
    }

    public class RelacionamentoViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageViewFoto;
        private TextView textViewNome;
        private TextView textViewTipo;
        private TextView textViewRaca;
        private TextView textViewSexo;
        private TextView textViewPedigree;
        private TextView textViewProcura;

        public RelacionamentoViewHolder(View itemView) {
            super(itemView);
            imageViewFoto = (ImageView) itemView.findViewById(R.id.imageFoto);
            textViewNome = (TextView) itemView.findViewById(R.id.textViewNome);
            textViewTipo = (TextView) itemView.findViewById(R.id.textViewTipo);
            textViewRaca = (TextView) itemView.findViewById(R.id.textViewRaca);
            textViewSexo = (TextView) itemView.findViewById(R.id.textViewSexo);
            textViewPedigree = (TextView) itemView.findViewById(R.id.textViewPedigree);
            textViewProcura = (TextView) itemView.findViewById(R.id.textViewProcura);
        }
    }
}

package com.powellapps.mymigo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.powellapps.mymigo.ImagemActivity;
import com.powellapps.mymigo.utils.ConstantsUtils;
import com.powellapps.mymigo.utils.PicassoUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import com.powellapps.mymigo.R;

/**
 * Created by raphaelramos on 10/06/17.
 */

public class AdapterGaleriaImagens extends RecyclerView.Adapter<AdapterGaleriaImagens.ImagemViewHolder>{

    private ArrayList<String> imagens;
    private Context context;

    public AdapterGaleriaImagens(Context context, ArrayList<String> imagens) {
        this.context = context;
        if(imagens == null){
            this.imagens = new ArrayList<>();
        }else {
            this.imagens = imagens;
        }
    }

    @Override
    public ImagemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_imagens, parent, false);
        return new ImagemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImagemViewHolder holder, int position) {
        final String imagem = imagens.get(position);
        PicassoUtils.padrao(context, imagem, holder.imageViewFoto);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(context, ImagemActivity.class);
                it.putExtra(ConstantsUtils.ANIMAL_IMAGEM, imagem);
                context.startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        try {
            return imagens.size();
        }catch (Exception e){
            return 0;
        }
    }

    public class ImagemViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageViewFoto;

        public ImagemViewHolder(View itemView) {
            super(itemView);
            imageViewFoto = (ImageView) itemView.findViewById(R.id.imageViewAnimal);
        }
    }
}
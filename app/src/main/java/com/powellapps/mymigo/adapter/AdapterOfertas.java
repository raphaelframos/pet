package com.powellapps.mymigo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.powellapps.mymigo.AnimalActivity;
import com.powellapps.mymigo.R;
import com.powellapps.mymigo.model.Oferta;
import com.powellapps.mymigo.model.TiposDeOfertas;
import com.powellapps.mymigo.utils.ConstantsUtils;
import com.powellapps.mymigo.utils.DateUtils;
import com.powellapps.mymigo.utils.GeralUtils;
import com.powellapps.mymigo.utils.PicassoUtils;

/**
 * Created by raphaelramos on 10/06/17.
 */

public class AdapterOfertas extends RecyclerView.Adapter<AdapterOfertas.OfertasViewHolder> {

    private ArrayList<Oferta> ofertas;
    private Context context;

    public AdapterOfertas(Context context, ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
        this.context = context;
    }

    @Override
    public OfertasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_ofertas, parent, false);
        OfertasViewHolder ofertasViewHolder = new OfertasViewHolder(view);
        return ofertasViewHolder;
    }

    @Override
    public void onBindViewHolder(OfertasViewHolder holder, int position) {
        try {
            final Oferta oferta = mostraOferta(holder, position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(context, AnimalActivity.class);
                    it.putExtra(ConstantsUtils.OFERTA, oferta);
                    context.startActivity(it);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @NonNull
    private Oferta mostraOferta(OfertasViewHolder holder, int position) {
        final Oferta oferta = ofertas.get(position);
        holder.textViewDescricao.setText(oferta.getTitulo());
        holder.textViewValor.setText(GeralUtils.formataDinheiro(context, oferta.getValor().toString()));
        holder.textViewData.setText(DateUtils.format(oferta.getData()));
        PicassoUtils.padrao(context, oferta.getAnimal().getImagem(), holder.imageViewFoto);
        mostraTiposDeOfertas(holder, oferta);
        mostraOfertaNova(holder, oferta);
        return oferta;
    }

    private void mostraOfertaNova(OfertasViewHolder holder, Oferta oferta) {
        if(oferta.getNovo()){
            holder.textViewNovo.setVisibility(View.VISIBLE);
        }else{
            holder.textViewNovo.setVisibility(View.GONE);
        }
    }

    private void mostraTiposDeOfertas(OfertasViewHolder holder, Oferta oferta) {
        int color = 0;
        String texto = "";
        if(oferta.getTipo().equalsIgnoreCase(TiposDeOfertas.COMPRA.toString())){
            color = ContextCompat.getColor(context, R.color.verde);
            texto = context.getString(R.string.compra);
        }else if(oferta.getTipo().equalsIgnoreCase(TiposDeOfertas.VENDA.toString())){
            color = ContextCompat.getColor(context, R.color.vermelho);
            texto = context.getString(R.string.venda);
        }else{
            color = ContextCompat.getColor(context, R.color.amarelo);
            texto = context.getString(R.string.doacao);
        }

        holder.textViewTipoDeOferta.setText(texto);
        holder.textViewTipoDeOferta.setTextColor(color);
    }

    @Override
    public int getItemCount() {
        return ofertas.size();
    }

    public void atualiza(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
        notifyDataSetChanged();
    }

    public class OfertasViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageViewFoto;
        private TextView textViewDescricao;
        private TextView textViewValor;
        private TextView textViewData;
        private TextView textViewTipoDeOferta;
        private TextView textViewNovo;

        public OfertasViewHolder(View itemView) {
            super(itemView);

            textViewDescricao = (TextView) itemView.findViewById(R.id.textViewDescricao);
            textViewValor = (TextView) itemView.findViewById(R.id.textViewValor);
            imageViewFoto = (ImageView) itemView.findViewById(R.id.imageViewOferta);
            textViewData = (TextView) itemView.findViewById(R.id.textViewData);
            textViewTipoDeOferta = (TextView) itemView.findViewById(R.id.textViewTipoDeOferta);
            textViewNovo = (TextView) itemView.findViewById(R.id.textViewNovo);
        }
    }
}

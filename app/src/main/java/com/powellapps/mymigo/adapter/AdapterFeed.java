package com.powellapps.mymigo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.powellapps.mymigo.R;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.utils.PicassoUtils;

import java.util.ArrayList;

/**
 * Created by raphaelramos on 19/06/17.
 */

public class AdapterFeed extends RecyclerView.Adapter<AdapterFeed.FeedViewHolder>{

    Context context;
    ArrayList<Animal> animais;

    public AdapterFeed(Context context, ArrayList<Animal> animais) {
        this.context = context;
        this.animais = animais;
    }

    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_feed, parent, false);
        return new FeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeedViewHolder holder, int position) {
        Animal animal = animais.get(position);
        PicassoUtils.customizado(context, animal.getImagem(), holder.imageViewFoto);
        holder.textViewRaca.setText(animal.getRaca());
    }

    @Override
    public int getItemCount() {
        return animais.size();
    }

    public void atualiza(ArrayList<Animal> animais) {
        this.animais = animais;
        notifyDataSetChanged();
    }

    public class FeedViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageViewFoto;
        private TextView textViewRaca;

        public FeedViewHolder(View itemView) {
            super(itemView);
            imageViewFoto = (ImageView) itemView.findViewById(R.id.imageViewImagemAnimal);
            textViewRaca = (TextView) itemView.findViewById(R.id.textViewRaca);
        }
    }
}

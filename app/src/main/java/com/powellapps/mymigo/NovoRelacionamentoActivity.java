package com.powellapps.mymigo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.model.Oferta;
import com.powellapps.mymigo.model.Singleton;
import com.powellapps.mymigo.model.TiposDeOfertas;
import com.powellapps.mymigo.utils.AlertaUtils;
import com.powellapps.mymigo.utils.ConstantsUtils;
import com.powellapps.mymigo.utils.FirebaseUtils;

import java.util.ArrayList;
import java.util.Calendar;

public class NovoRelacionamentoActivity extends AppCompatActivity {

    private Spinner spinnerAnimal;
    private EditText editTextProcura;
    private ArrayList<Animal> animais;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_relacionamento);

        spinnerAnimal = (Spinner) findViewById(R.id.spinnerAnimal);
        editTextProcura = (EditText) findViewById(R.id.editTextProcura);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Buscando animais...");
        progressDialog.show();

        DatabaseReference myRef = FirebaseUtils.getReferenceAnimaisDoProprietario();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                animais = new ArrayList<>();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    final Animal animal = postSnapshot.getValue(Animal.class);
                    animais.add(animal);
                }
                atualiza(animais);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                animais = new ArrayList<>();
                atualiza(animais);
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_salvar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_salvar:
                salva();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void salva(){
        ProgressDialog progress = new ProgressDialog(NovoRelacionamentoActivity.this);
        progress.setCancelable(false);
        progress.setMessage("Salvando relacionamento...");
        progress.show();

        String procura = editTextProcura.getText().toString();
        Animal animal = (Animal) spinnerAnimal.getSelectedItem();
        Oferta oferta = new Oferta();
        oferta.setStatus(TiposDeOfertas.ATIVO.name());
        oferta.setAnimal(animal);
        oferta.setIdProprietario(Singleton.getInstance().getId());
        oferta.setProcura(procura);
        oferta.setNovo(true);
        oferta.setData(Calendar.getInstance().getTimeInMillis());
        DatabaseReference ref = FirebaseUtils.getReferenceRelacionamento();
        ref.push().setValue(oferta);
        fechaProgress(progress);

    }

    private void fechaProgress(ProgressDialog progressDialog){
        try{
            progressDialog.dismiss();
            AlertaUtils.alertaComUmaOpcao(NovoRelacionamentoActivity.this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }, "Relacionamento adicionado com sucesso.");
        }catch (Exception e){}
    }

    private void atualiza(ArrayList<Animal> animais) {
        ArrayAdapter<Animal> adapter =
                new ArrayAdapter<Animal>(this, android.R.layout.simple_spinner_dropdown_item, animais);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAnimal.setAdapter(adapter);
    }
}

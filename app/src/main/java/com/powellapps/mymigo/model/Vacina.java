package com.powellapps.mymigo.model;

import java.io.Serializable;

/**
 * Created by raphaelramos on 17/06/17.
 */

public class Vacina implements Serializable{

    private String nome;
    private Long data;
    private Boolean vacinado;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getData() {
        return data;
    }

    public void setData(Long data) {
        this.data = data;
    }

    public Boolean getVacinado() {
        return vacinado;
    }

    public void setVacinado(Boolean vacinado) {
        this.vacinado = vacinado;
    }
}

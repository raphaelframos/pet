package com.powellapps.mymigo.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by raphaelramos on 09/06/17.
 */

public class Animal implements Serializable{

    private String nome;
    private String raca;
    private Long nascimento;
    private ArrayList<String> imagens;
    private String tipo;
    private String sexo;
    private Boolean pedigree;
    private String tamanho;
    private String cor;
    private String porte;
    private String imagemPrincipal;
    private ArrayList<Vacina> vacinas;
    private String idProprietario;
    private Long dataCriacao;
    private Boolean castrado;

    public Long getNascimento() {
        return nascimento;
    }

    public void setNascimento(Long nascimento) {
        this.nascimento = nascimento;
    }

    public String getPorte() {
        return porte;
    }

    public void setPorte(String porte) {
        this.porte = porte;
    }
    
    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Boolean getPedigree() {
        return pedigree;
    }

    public void setPedigree(Boolean pedigree) {
        this.pedigree = pedigree;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public ArrayList<String> getImagens() {
        return imagens;
    }

    public void setImagens(ArrayList<String> imagem) {
        this.imagens = imagem;
    }

    public void adicionaImagem(String imagem) {
        if(imagens == null){
            imagens = new ArrayList<>();
        }
        imagens.add(imagem);
    }

    public String getImagem() {
        try{
            return imagens.get(0);
        }catch (Exception e){
            return "";
        }
    }

    public ArrayList<Vacina> getVacinas() {
        return vacinas;
    }

    public void setVacinas(ArrayList<Vacina> vacinas) {
        this.vacinas = vacinas;
    }

    public String getImagemPrincipal() {
        return imagemPrincipal;
    }

    public void setImagemPrincipal(String imagemPrincipal) {
        this.imagemPrincipal = imagemPrincipal;
    }

    public String getIdProprietario() {
        return idProprietario;
    }

    public void setIdProprietario(String idProprietario) {
        this.idProprietario = idProprietario;
    }

    public Long getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Long dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    @Override
    public String toString() {
        return nome + " - " + raca;
    }

    public Boolean getCastrado() {
        return castrado;
    }

    public void setCastrado(Boolean castrado) {
        this.castrado = castrado;
    }
}

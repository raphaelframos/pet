package com.powellapps.mymigo.model;

/**
 * Created by raphaelramos on 17/06/17.
 */

public class Singleton {

    private static Singleton instance;
    private String id = "3SUK2vtvNWbFaZSipGITaKeYOC42";

    public static Singleton getInstance(){
        if(instance == null){
            instance = new Singleton();
        }
        return instance;
    }

    public String getId(){
        return id;
    }
}

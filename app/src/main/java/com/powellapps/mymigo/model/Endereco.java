package com.powellapps.mymigo.model;

import java.io.Serializable;

/**
 * Created by raphaelramos on 10/06/17.
 */

public class Endereco implements Serializable{

    private String rua;
    private String numero;
    private String bairro;
    private String CEP;
    private String cidade;
    private String estado;
    private String observacao;
    private String pais;

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCEP() {
        return CEP;
    }

    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDemonstracao() {
        return getRua() + ", " + getNumero() + ", " + getBairro() + ", " + getCidade() + "-" + getEstado();
    }
}

package com.powellapps.mymigo.model;

/**
 * Created by raphaelramos on 13/06/17.
 */

public enum TiposDeOfertas {

    VENDA, COMPRA, DOACAO, ATIVO;

}

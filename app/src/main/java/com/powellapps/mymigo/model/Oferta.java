package com.powellapps.mymigo.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by raphaelramos on 10/06/17.
 */

public class Oferta implements Serializable{

    private String descricao;
    private String titulo;
    private Double valor;
    private String procura;
    private Long data;
    private Animal animal;
    private String tipo;
    private Boolean novo;
    private String status;
    private String idProprietario;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Long getData() {
        return data;
    }

    public void setData(Long data) {
        this.data = data;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public String getProcura() {
        return procura;
    }

    public void setProcura(String procura) {
        this.procura = procura;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Boolean getNovo() {
        return novo;
    }

    public void setNovo(Boolean novo) {
        this.novo = novo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdProprietario() {
        return idProprietario;
    }

    public void setIdProprietario(String idProprietario) {
        this.idProprietario = idProprietario;
    }
}

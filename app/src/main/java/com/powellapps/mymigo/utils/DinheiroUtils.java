package com.powellapps.mymigo.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by raphael on 05/09/15.
 */
public class DinheiroUtils {

    public static String formataValorDemonstracao(BigDecimal valor, int decimais) {

        DecimalFormat format = new DecimalFormat();

        if (decimais == 0) {
            format.applyPattern("#,###,##0");
        } else if (decimais == 1) {
            format.applyPattern("#,###,##0.0");
        } else if (decimais == 2) {
            format.applyPattern("#,###,##0.00");
        } else if (decimais == 3) {
            format.applyPattern("#,###,##0.000");
        } else if (decimais == 4) {
            format.applyPattern("#,###,##0.0000");
        } else if (decimais == 5) {
            format.applyPattern("#,###,##0.00000");
        }
        return format.format(valor);
    }

    public static String formataValorDemonstracao(BigDecimal valor) {
        return formataValorDemonstracao(valor,2);
    }

    public static String formataValorDemonstracao(String value) {
        BigDecimal newValue = BigDecimal.ZERO;
        try {
            newValue = new BigDecimal(value);
        }catch (Exception e){
            e.printStackTrace();
            newValue = BigDecimal.ZERO;
        }
        return formataValorDemonstracao(newValue);
    }
}

package com.powellapps.mymigo.utils;

import android.content.Context;
import android.util.Log;

import java.math.BigDecimal;

import com.powellapps.mymigo.R;

/**
 * Created by raphaelramos on 10/06/17.
 */

public class GeralUtils {
    private static final String TAG = "socialpet";

    public static String getSimOuNao(Boolean valor, Context applicationContext) {
        if(valor){
            return applicationContext.getString(R.string.sim);
        }

        return applicationContext.getString(R.string.nao);
    }

    public static String formataDinheiro(Context context, BigDecimal valor) {
        return context.getString(R.string.moeda) + " " + DinheiroUtils.formataValorDemonstracao(valor);
    }

    public static String formataDinheiro(Context context, String valor) {
        return formataDinheiro(context, new BigDecimal(valor));
    }

    public static void mostra(String valor) {
        Log.v(TAG, valor);
    }
}

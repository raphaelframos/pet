package com.powellapps.mymigo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.powellapps.mymigo.R;


public class AlertaUtils {

    private AlertDialog.Builder alert;
    private Activity activity;
   // private SnackBar snackBar;

    public AlertaUtils(Activity activity){
        alert = new AlertDialog.Builder(activity, R.style.Dialog);
        this.activity = activity;
    }

    public AlertaUtils setTitulo(String titulo) {
        alert.setTitle(titulo);
        return this;
    }

    public AlertaUtils setMensagem(String mensagem){
        alert.setMessage(mensagem);
        return this;
    }

    public AlertaUtils setTitulo(int idTitulo) {
        alert.setTitle(activity.getString(idTitulo));
        return this;
    }

    /*
    public void criaSnackPadrao(String mensagem){
        snackBar = new SnackBar(activity, mensagem, activity.getResources().getString(android.R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();
            }
        });
        snackBar.setBackgroundSnackBar(activity.getResources().getColor(R.color.action_bar));
        snackBar.setColorButton(activity.getResources().getColor(R.color.branco));
        snackBar.show();
    }

    public void criaSnackPadraoComTempo(String mensagem, int tempo){
        snackBar = new SnackBar(activity, mensagem, activity.getResources().getString(android.R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();
            }
        });
        snackBar.setBackgroundSnackBar(activity.getResources().getColor(R.color.action_bar));
        snackBar.setColorButton(activity.getResources().getColor(R.color.branco));
        snackBar.setDismissTimer(tempo);
        snackBar.show();
    }

    public AlertaUtils criaSnackSimples(String mensagem){
        snackBar = new SnackBar(activity, mensagem);
        return this;
    }

    public AlertaUtils criaSnack(String mensagem, String textoBotao, View.OnClickListener onClick){
        snackBar = new SnackBar(activity, mensagem, textoBotao,onClick);
        return this;
    }

    public AlertaUtils setBackgroundSnack(int cor){
        snackBar.setBackgroundSnackBar(cor);
        return this;
    }

    public AlertaUtils setCorBotaoSnack(int cor){
        snackBar.setColorButton(cor);
        return this;
    }

    */

    public AlertaUtils setMensagem(int idMensagem){
        alert.setMessage(activity.getString(idMensagem));
        return this;
    }

    public static void mostraErroNo(EditText edit, String erro){
        edit.setError(erro);
    }

    public static void mostraErroNo(EditText edit, String erro, Drawable icone){
        edit.setError(erro, icone);
    }

    public AlertaUtils adicionaButtonPositivo(int id, OnClickListener listener){
        alert.setPositiveButton(activity.getResources().getString(id), listener);
        return this;
    }

    public AlertaUtils adicionaButtonNegativo(int id, OnClickListener listener){
        alert.setNegativeButton(activity.getResources().getString(id), listener);
        return this;
    }

    public AlertaUtils adicionaButtonNeutro(int id, OnClickListener listener){
        alert.setNeutralButton(activity.getResources().getString(id), listener);
        return this;
    }

    public AlertaUtils adicionaIcone(int idIcone){
        alert.setIcon(activity.getResources().getDrawable(idIcone));
        return this;
    }
    public void mostrar() {
        try{
            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public AlertaUtils adicionaTitulo(String titulo){
        alert.setTitle(titulo);
        return this;
    }

    public AlertaUtils adicionaMensagem(String mensagem){
        alert.setMessage(mensagem);
        return this;
    }

    public AlertaUtils adicionaButtonPositivo(OnClickListener eventoSim) {
        alert.setPositiveButton(activity.getString(R.string.sim), eventoSim);
        return this;
    }

    public AlertaUtils adicionaButtonNegativo(OnClickListener eventoNao) {
        alert.setNegativeButton(activity.getString(R.string.nao), eventoNao);
        return this;
    }

    public AlertaUtils adicionaButtonNeutro(String mensagem, OnClickListener listener) {
        alert.setNeutralButton(mensagem, listener);
        return this;
    }

    public AlertaUtils adicionaButtonPositivo(String texto, OnClickListener listener) {
        alert.setPositiveButton(texto, listener);
        return this;
    }

    public AlertaUtils adicionaButtonNegativo(String texto, OnClickListener listener) {
        alert.setNegativeButton(texto, listener);
        return this;
    }

    public AlertaUtils setCancelavel(boolean cancelavel) {
        alert.setCancelable(cancelavel);
        return this;
    }

    public static void mostraToast(Context context, String mensagem) {
        Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show();
    }

    public static void criaToastTopLongo(Activity activity, String mensagem){
        Toast toast = Toast.makeText(activity,mensagem,Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP,0,0);
        toast.show();
    }

    public static void criaToastCentroLongo(Activity activity, String mensagem){
        Toast toast = Toast.makeText(activity,mensagem,Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }

    public static void alertaDuasOpcoes(final Activity activity, String textoUm, OnClickListener listenerUm, String textoDois, OnClickListener listenerDois, String titulo, String mensagem) {
        new AlertaUtils(activity).adicionaTitulo(titulo).adicionaMensagem(mensagem).
                adicionaButtonPositivo(textoUm, listenerUm).adicionaButtonNegativo(textoDois,listenerDois).mostrar();

    }

    public static void alertaComFaltaDeConexao(final Activity activity) {
        new AlertaUtils(activity).adicionaButtonNeutro(activity.getResources().getString(android.R.string.ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                activity.finish();
            }
        }).setCancelavel(false).setTitulo("Conexão").setMensagem("Sua conexão com a internet não está ativada!").mostrar();
    }

    public static void alertaComUmaOpcao(final Activity activity, OnClickListener listenerUm, String titulo, String mensagem) {
        new AlertaUtils(activity).adicionaButtonNeutro(activity.getResources().getString(android.R.string.ok), listenerUm).setCancelavel(false).setTitulo(titulo).setMensagem(mensagem).mostrar();
    }


    public static void alertaComUmaOpcao(Activity activity, OnClickListener onClickListener, String mensagem) {
        alertaComUmaOpcao(activity,onClickListener,activity.getString(R.string.app_name),mensagem);
    }
}
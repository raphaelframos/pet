package com.powellapps.mymigo.utils;

import android.support.v4.app.Fragment;

import com.powellapps.mymigo.MainActivity;
import com.powellapps.mymigo.R;

/**
 * Created by raphaelramos on 09/06/17.
 */

public class FragmentUtils {
    public static void replace(MainActivity activity, Fragment fragment) {
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.content, fragment).commit();
    }
}

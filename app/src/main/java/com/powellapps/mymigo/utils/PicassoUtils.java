package com.powellapps.mymigo.utils;

import android.content.Context;
import android.widget.ImageView;

import com.powellapps.mymigo.R;
import com.squareup.picasso.Picasso;

/**
 * Created by raphaelramos on 10/06/17.
 */

public class PicassoUtils {
    public static void padrao(Context context, String imagem, ImageView imageView) {
        show(context,imagem,imageView, 200);

    }

    public static void customizado(Context context, String imagem, ImageView imageView, int tamanho){
        show(context, imagem, imageView, tamanho);
    }

    private static void show(Context context, String imagem, ImageView imageView, int tamanho){
        try{
            Picasso.with(context).load(imagem)
                    .resize(tamanho, tamanho)
                    .transform(CircleTransform.getArredondado()).into(imageView);
        }catch (Exception e){
        }
    }
    public static void padraoMaior(Context context, String imagem, ImageView imageView) {
        show(context,imagem,imageView, 250);
    }

    public static void customizado(Context context, String imagem, ImageView imageView) {
        try{
            Picasso.with(context).load(imagem).into(imageView);
        }catch (Exception e){
        }
    }
}

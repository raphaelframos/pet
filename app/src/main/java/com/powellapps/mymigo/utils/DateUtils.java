package com.powellapps.mymigo.utils;

import android.content.Intent;
import android.text.TextWatcher;

import java.security.spec.ECField;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by raphaelramos on 17/06/17.
 */

public class DateUtils {

    public static Long format(String data) {
        long dataConvertida;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = sdf.parse(data);
            dataConvertida = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            dataConvertida = 0l;
        }
        return dataConvertida;
    }

    public static String format(Long nascimento) {
        try{
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            Date today = new Date(nascimento);
            String reportDate = df.format(today);
            return reportDate;
        }catch (Exception e){
            return format(Calendar.getInstance().getTimeInMillis());
        }
    }

    public static Integer getIdade(Long dataLong) {
        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(new Date(dataLong));
        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(Calendar.getInstance().getTime());

        int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
        int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
        return diffMonth;
    }
}

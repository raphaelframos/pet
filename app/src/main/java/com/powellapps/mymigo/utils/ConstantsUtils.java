package com.powellapps.mymigo.utils;

/**
 * Created by raphaelramos on 10/06/17.
 */

public class ConstantsUtils {
    public static final String ANIMAL = "animal";
    public static final String OFERTA = "oferta";
    public static final String REF_ANIMAIS = "animais";
    public static final String REF_IMAGENS = "imagens/animais";
    public static final String STORAGE_IMAGENS = "gs://meu-amigo-1b4c5.appspot.com";
    public static final String ANIMAL_IMAGEM = "animal_imagem";
    public static final String ID_PROPRIETARIO = "idProprietario";
    public static final String REF_OFERTAS = "ofertas";
    public static final String STATUS = "status";
    public static final String REF_RELACIONAMENTO = "relacionamentos";
    public static final String DATA_CRIACAO = "dataCriacao";
}

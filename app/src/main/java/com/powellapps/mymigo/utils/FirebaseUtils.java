package com.powellapps.mymigo.utils;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.powellapps.mymigo.model.Singleton;

/**
 * Created by raphaelramos on 18/06/17.
 */

public class FirebaseUtils {

    public static DatabaseReference getReferenceAnimaisDoProprietario() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ConstantsUtils.REF_ANIMAIS)
                .equalTo(ConstantsUtils.ID_PROPRIETARIO, Singleton.getInstance().getId()).getRef();
        return myRef;
    }

    public static DatabaseReference getReferenceRelacionamento() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ConstantsUtils.REF_RELACIONAMENTO);
        return myRef;
    }

    public static DatabaseReference getReferenceAnimais() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ConstantsUtils.REF_ANIMAIS).getRef();
        return myRef;
    }
}

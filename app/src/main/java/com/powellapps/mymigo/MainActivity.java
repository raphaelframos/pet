package com.powellapps.mymigo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.powellapps.mymigo.fragments.NovidadesFragment;
import com.powellapps.mymigo.fragments.MeusAnimaisFragment;
import com.powellapps.mymigo.fragments.OfertasFragment;
import com.powellapps.mymigo.fragments.RelacionamentoFragment;
import com.powellapps.mymigo.utils.FragmentUtils;


public class MainActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_feed:
                    FragmentUtils.replace(MainActivity.this, new NovidadesFragment());
                    return true;
                case R.id.navigation_ofertas:
                    FragmentUtils.replace(MainActivity.this, new OfertasFragment());
                    return true;
                case R.id.navigation_relacionamento:
                    FragmentUtils.replace(MainActivity.this, new RelacionamentoFragment());
                    return true;
                case R.id.navigation_meus:
                    FragmentUtils.replace(MainActivity.this, new MeusAnimaisFragment());
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        FragmentUtils.replace(MainActivity.this, new NovidadesFragment());

    }

}

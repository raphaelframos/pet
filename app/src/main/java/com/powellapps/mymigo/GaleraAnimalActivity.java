package com.powellapps.mymigo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.powellapps.mymigo.adapter.AdapterGaleriaImagens;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.utils.ConstantsUtils;


public class GaleraAnimalActivity extends AppCompatActivity {


    private RecyclerView recyclerViewImagens;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galera_animal);

        Animal animal = (Animal) getIntent().getSerializableExtra(ConstantsUtils.ANIMAL);

        Log.v("", "Imagens " + animal.getImagens());
        recyclerViewImagens = (RecyclerView) findViewById(R.id.recycler_galeria_imagens);
        recyclerViewImagens.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewImagens.setAdapter(new AdapterGaleriaImagens(this, animal.getImagens()));
    }
}

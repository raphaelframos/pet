package com.powellapps.mymigo;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.powellapps.mymigo.adapter.AdapterGaleriaImagens;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.model.Oferta;
import com.powellapps.mymigo.model.Usuario;
import com.powellapps.mymigo.utils.ConstantsUtils;
import com.powellapps.mymigo.utils.DateUtils;
import com.powellapps.mymigo.utils.GeralUtils;
import com.powellapps.mymigo.utils.PicassoUtils;

import java.util.ArrayList;

public class AnimalActivity extends AppCompatActivity {

    private Animal animal;
    private ImageView imageViewFoto;
    private TextView textViewNome;
    private TextView textViewTipo;
    private TextView textViewRaca;
    private TextView textViewSexo;
    private TextView textViewIdade;
    private TextView textViewTamanho;
    private TextView textViewCor;
    private TextView textViewPedigree;
    private LinearLayout linearLayoutFundoAnimal;
    private LinearLayout linearLayoutFundoOferta;
    private TextView textViewNomeUsuario;
    private TextView textViewEmailUsuario;
    private TextView textViewTelefoneUsuario;
    private TextView textViewEnderecoUsuario;
    private TextView textViewDescricao;
    private ImageView imageViewFotoUsuario;
    private TextView textViewData;
    private TextView textViewValor;
    private TextView textViewTitulo;
    private Oferta oferta;
    private RecyclerView recyclerViewImagens;
    private ArrayList<String> imagens;
    private TextView textViewTipoOferta;
    //private TextView

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);
        animal = (Animal) getIntent().getSerializableExtra(ConstantsUtils.ANIMAL);
        oferta = (Oferta) getIntent().getSerializableExtra(ConstantsUtils.OFERTA);

        atribuiViews();
        if(ehOferta()){
            imagens = oferta.getAnimal().getImagens();
            linearLayoutFundoOferta.setVisibility(View.VISIBLE);
            mostraOferta();
        }else{
            imagens = animal.getImagens();
            linearLayoutFundoOferta.setVisibility(View.GONE);
            mostraAnimal(animal);
        }

        AdapterGaleriaImagens adapterGaleriaImagens = new AdapterGaleriaImagens(this, imagens);
        recyclerViewImagens.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewImagens.setAdapter(adapterGaleriaImagens);


        imageViewFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), GaleraAnimalActivity.class);
                intent.putExtra(ConstantsUtils.ANIMAL, animal);
                startActivity(intent);
            }
        });
    }

    private void mostraOferta() {
        mostraAnimal(oferta.getAnimal());
        textViewTitulo.setText(oferta.getTitulo());
        textViewDescricao.setText(oferta.getDescricao());
        textViewTipoOferta.setText(oferta.getTipo());
        textViewData.setText(DateUtils.format(oferta.getData()));
        textViewValor.setText(GeralUtils.formataDinheiro(getApplicationContext(), oferta.getValor().toString()));
       // mostraUsuario(oferta.getUsuario());
    }

    private void mostraUsuario(Usuario usuario) {
        textViewNomeUsuario.setText(usuario.getNomeCompleto());
        textViewTelefoneUsuario.setText(usuario.getTelefone());
        textViewEmailUsuario.setText(usuario.getEmail());
        PicassoUtils.padrao(getApplicationContext(),usuario.getImagem(),imageViewFotoUsuario);
        textViewEnderecoUsuario.setText(usuario.getEndereco().getDemonstracao());
    }

    private boolean ehOferta(){
        return oferta != null;
    }

    private void mostraAnimal(Animal animal){
        PicassoUtils.padraoMaior(this, animal.getImagem(), imageViewFoto);
        textViewNome.setText(animal.getNome());
        textViewTipo.setText(animal.getTipo());
        textViewRaca.setText(animal.getRaca());
        textViewSexo.setText(animal.getSexo());
        textViewIdade.setText(DateUtils.getIdade(animal.getNascimento()) + " " + getString(R.string.meses));
        textViewTamanho.setText(animal.getTamanho());
        textViewCor.setText(animal.getCor());
        textViewPedigree.setText(GeralUtils.getSimOuNao(animal.getPedigree(), getApplicationContext()));
        if(animal.getSexo().equalsIgnoreCase(getString(R.string.macho))){
            linearLayoutFundoAnimal.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.azul_claro));
            linearLayoutFundoOferta.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.azul_claro));
        }else{
            linearLayoutFundoAnimal.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.rosa));
            linearLayoutFundoOferta.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.rosa));
        }
    }

    private void atribuiViews(){
        imageViewFoto = (ImageView) findViewById(R.id.imageViewFotoAnimal);
        textViewCor = (TextView) findViewById(R.id.textViewCor);
        textViewIdade = (TextView) findViewById(R.id.textViewIdade);
        textViewNome = (TextView) findViewById(R.id.textViewNomeAnimal);
        textViewPedigree = (TextView) findViewById(R.id.textViewPedigreeAnimal);
        textViewRaca = (TextView) findViewById(R.id.textViewRacaAnimal);
        textViewSexo = (TextView) findViewById(R.id.textViewSexoAnimal);
        textViewTamanho = (TextView) findViewById(R.id.textViewTamanho);
        textViewTipo = (TextView) findViewById(R.id.textViewTipoAnimal);
        linearLayoutFundoAnimal = (LinearLayout) findViewById(R.id.linearLayoutFundoAnimal);
        linearLayoutFundoOferta = (LinearLayout) findViewById(R.id.linearLayoutOferta);
        imageViewFotoUsuario = (ImageView) findViewById(R.id.imageViewFotoUsuario);
        textViewDescricao = (TextView) findViewById(R.id.textViewDescricao);
        textViewEmailUsuario = (TextView) findViewById(R.id.textViewEmail);
        textViewTelefoneUsuario = (TextView) findViewById(R.id.textViewTelefone);
        textViewEnderecoUsuario = (TextView) findViewById(R.id.textViewEndereco);
        textViewValor = (TextView) findViewById(R.id.textViewValorOferta);
        textViewData = (TextView) findViewById(R.id.textViewData);
        textViewTipoOferta = (TextView) findViewById(R.id.textViewTipoOferta);
        textViewNomeUsuario = (TextView) findViewById(R.id.textViewNomeUsuario);
        recyclerViewImagens = (RecyclerView) findViewById(R.id.recycler_album_animal);
        textViewTitulo = (TextView) findViewById(R.id.textViewTitulo);
    }
}

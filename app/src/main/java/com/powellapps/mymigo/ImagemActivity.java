package com.powellapps.mymigo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.powellapps.mymigo.utils.ConstantsUtils;
import com.powellapps.mymigo.utils.GeralUtils;
import com.powellapps.mymigo.utils.PicassoUtils;
import com.squareup.picasso.Picasso;

public class ImagemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagem);
        ImageView imageView = (ImageView) findViewById(R.id.imageViewImagem);
        String imagem = getIntent().getStringExtra(ConstantsUtils.ANIMAL_IMAGEM);
        GeralUtils.mostra(imagem);
        Picasso.with(this).load(imagem).into(imageView);
    }
}

package com.powellapps.mymigo.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;

import com.powellapps.mymigo.R;
import com.powellapps.mymigo.model.Vacina;
import com.powellapps.mymigo.utils.DateUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class NovaVacinaDialogFragment extends DialogFragment {

    private AtualizaLista callback;
    private Vacina vacina;
    private EditText editTextNome;
    private EditText editTextData;
    private Switch switchVacinado;

    public NovaVacinaDialogFragment() {
        // Required empty public constructor
    }

    public void setCallback(AtualizaLista atualizaLista){
        this.callback = atualizaLista;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nova_vacina_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbar);

        editTextData = (EditText) getView().findViewById(R.id.editTextDataVacina);
        editTextNome = (EditText) getView().findViewById(R.id.editTextNomeVacina);
        switchVacinado = (Switch) getView().findViewById(R.id.switchVacinado);

        toolbar.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()){
                            case R.id.action_salvar:
                                if(vacina == null){
                                    vacina = new Vacina();
                                }

                                vacina.setNome(editTextNome.getText().toString());
                                vacina.setData(DateUtils.format(editTextData.getText().toString()));
                                vacina.setVacinado(switchVacinado.isChecked());
                                callback.atualiza(vacina);
                                dismiss();
                                break;
                        }
                        return true;
                    }
                });
        toolbar.inflateMenu(R.menu.menu_salvar);
    }

    public interface AtualizaLista{
        void atualiza(Vacina vacina);
    }
}

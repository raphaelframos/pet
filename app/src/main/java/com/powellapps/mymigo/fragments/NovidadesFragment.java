package com.powellapps.mymigo.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.powellapps.mymigo.LoginActivity;
import com.powellapps.mymigo.NovoAnimalActivity;
import com.powellapps.mymigo.R;
import com.powellapps.mymigo.adapter.AdapterFeed;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.utils.ConstantsUtils;
import com.powellapps.mymigo.utils.FirebaseUtils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NovidadesFragment extends BaseFragment {

    private RecyclerView recyclerViewFeed;
    private AdapterFeed adapterFeed;
    private ArrayList<Animal> animais = new ArrayList<>();
    private FloatingActionButton fabNovoAnimal;

    public NovidadesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_novidades, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerViewFeed = (RecyclerView) getView().findViewById(R.id.recycler_feed);
        fabNovoAnimal = (FloatingActionButton) getView().findViewById(R.id.fabNovoAnimalFeed);
        recyclerViewFeed.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterFeed = new AdapterFeed(getContext(), animais);
        recyclerViewFeed.setAdapter(adapterFeed);

        fabNovoAnimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  startActivity(new Intent(getActivity(), NovoAnimalActivity.class));
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Buscando os últimos animais cadastrados");
        progressDialog.show();
        DatabaseReference myRef = FirebaseUtils.getReferenceAnimais();
        myRef.orderByChild(ConstantsUtils.DATA_CRIACAO);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                animais = new ArrayList<>();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    final Animal animal = postSnapshot.getValue(Animal.class);
                    animais.add(animal);
                }
                adapterFeed.atualiza(animais);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                animais = new ArrayList<>();
                adapterFeed.atualiza(animais);
                progressDialog.dismiss();
            }
        });

    }
}

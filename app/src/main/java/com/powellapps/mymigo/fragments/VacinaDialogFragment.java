package com.powellapps.mymigo.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.powellapps.mymigo.NovoAnimalActivity;
import com.powellapps.mymigo.R;
import com.powellapps.mymigo.adapter.AdapterVacina;
import com.powellapps.mymigo.model.Vacina;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class VacinaDialogFragment extends DialogFragment implements NovaVacinaDialogFragment.AtualizaLista{


    private ArrayList<Vacina> vacinas = new ArrayList<>();
    private AdapterVacina adapterVacina;
    private VacinaCallback vacinaCallback;

    public VacinaDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vacina_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbar);
        RecyclerView recyclerViewVacinas = (RecyclerView) getView().findViewById(R.id.recycler_vacinas);
        recyclerViewVacinas.setLayoutManager(new LinearLayoutManager(getContext()));

        adapterVacina = new AdapterVacina(getActivity(), vacinas);
        recyclerViewVacinas.setAdapter(adapterVacina);

        toolbar.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Handle the menu item

                        switch (item.getItemId()){

                            case R.id.action_salvar:
                                vacinaCallback.adiciona(vacinas);
                                dismiss();
                                break;

                            case R.id.action_nova_vacina:
                                NovaVacinaDialogFragment novaVacinaDialogFragment = new NovaVacinaDialogFragment();
                                novaVacinaDialogFragment.setCallback(VacinaDialogFragment.this);
                                        novaVacinaDialogFragment.show(getFragmentManager(), "novavacina");
                                break;
                        }
                        return true;
                    }
                });
        toolbar.inflateMenu(R.menu.menu_vacina);
    }

    @Override
    public void atualiza(Vacina vacina) {
        adapterVacina.adiciona(vacina);
    }

    public void setCallback(VacinaCallback vacinaCallback) {
        this.vacinaCallback = vacinaCallback;
    }

    public interface VacinaCallback{
        void adiciona(ArrayList<Vacina> vacinas);
    }
}

package com.powellapps.mymigo.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.powellapps.mymigo.NovoRelacionamentoActivity;
import com.powellapps.mymigo.R;
import com.powellapps.mymigo.adapter.AdapterRelacionamento;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.model.Oferta;
import com.powellapps.mymigo.model.TiposDeOfertas;
import com.powellapps.mymigo.utils.FirebaseUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class RelacionamentoFragment extends BaseFragment {


    private RecyclerView recyclerViewRelacionamento;
    private AdapterRelacionamento adapterRelacionamento;
    private ArrayList<Oferta> ofertas;
    private FloatingActionButton fabNovo;

    public RelacionamentoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_relacionamento, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerViewRelacionamento = (RecyclerView) getView().findViewById(R.id.recycler_relacionamento);
        fabNovo = (FloatingActionButton) getView().findViewById(R.id.fabNovoRelacionamento);
        recyclerViewRelacionamento.setLayoutManager(new LinearLayoutManager(getContext()));

        ofertas = new ArrayList<>();
        adapterRelacionamento = new AdapterRelacionamento(ofertas, getContext());
        recyclerViewRelacionamento.setAdapter(adapterRelacionamento);
        fabNovo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), NovoRelacionamentoActivity.class));
            }
        });

        final ProgressDialog progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage("Buscando relacionamentos...");
        progress.show();
        DatabaseReference myRef = FirebaseUtils.getReferenceRelacionamento();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ofertas = new ArrayList<>();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    final Oferta animal = postSnapshot.getValue(Oferta.class);
                    ofertas.add(animal);
                }
                adapterRelacionamento.atualiza(ofertas);
                progress.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                ofertas = new ArrayList<>();
                adapterRelacionamento.atualiza(ofertas);
                progress.dismiss();
            }
        });
    }
}

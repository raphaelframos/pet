package com.powellapps.mymigo.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.powellapps.mymigo.NovaOfertaActivity;
import com.powellapps.mymigo.R;
import com.powellapps.mymigo.adapter.AdapterOfertas;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.model.Endereco;
import com.powellapps.mymigo.model.Oferta;
import com.powellapps.mymigo.model.Singleton;
import com.powellapps.mymigo.model.TiposDeOfertas;
import com.powellapps.mymigo.model.Usuario;
import com.powellapps.mymigo.utils.ConstantsUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class OfertasFragment extends BaseFragment {

    private FloatingActionButton fabNovaOferta;
    private ArrayList<Oferta> ofertas;
    private RecyclerView recyclerViewOfertas;


    public OfertasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ofertas, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fabNovaOferta = (FloatingActionButton) getView().findViewById(R.id.fabNovaOferta);
        recyclerViewOfertas = (RecyclerView) getView().findViewById(R.id.recycler_ofertas);

        ofertas = new ArrayList<>();
        recyclerViewOfertas.setLayoutManager(new LinearLayoutManager(getContext()));
        final AdapterOfertas adapterOfertas = new AdapterOfertas(getContext(), ofertas);
        recyclerViewOfertas.setAdapter(adapterOfertas);

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Buscando ofertas...");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ConstantsUtils.REF_OFERTAS).equalTo(ConstantsUtils.STATUS, TiposDeOfertas.ATIVO.toString()).getRef();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ofertas = new ArrayList<>();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    final Oferta oferta = postSnapshot.getValue(Oferta.class);
                    ofertas.add(oferta);
                }
                adapterOfertas.atualiza(ofertas);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                ofertas = new ArrayList<>();
                adapterOfertas.atualiza(ofertas);
                progressDialog.dismiss();
            }
        });


        fabNovaOferta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), NovaOfertaActivity.class));
            }
        });
    }
}

package com.powellapps.mymigo.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.ArrayList;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StreamDownloadTask;
import com.powellapps.mymigo.NovoAnimalActivity;
import com.powellapps.mymigo.PerfilActivity;
import com.powellapps.mymigo.R;
import com.powellapps.mymigo.adapter.AdapterMeusAnimais;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.model.Singleton;
import com.powellapps.mymigo.utils.ConstantsUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeusAnimaisFragment extends BaseFragment {

    private RecyclerView recyclerViewMeusAnimais;
    private AdapterMeusAnimais adapterMeusAnimais;
    private ArrayList<Animal> animais;
    private FloatingActionButton fabNovoAnimal;

    public MeusAnimaisFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meus_animais, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerViewMeusAnimais = (RecyclerView) getView().findViewById(R.id.recycler_meus_animais);
        fabNovoAnimal = (FloatingActionButton) getView().findViewById(R.id.fabNovoAnimal);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerViewMeusAnimais.setLayoutManager(llm);

        setHasOptionsMenu(true);
        fabNovoAnimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), NovoAnimalActivity.class));
            }
        });

        animais = new ArrayList<>();

        adapterMeusAnimais = new AdapterMeusAnimais(getContext(), animais);
        recyclerViewMeusAnimais.setAdapter(adapterMeusAnimais);

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Buscando meus animais...");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ConstantsUtils.REF_ANIMAIS).equalTo(ConstantsUtils.ID_PROPRIETARIO, Singleton.getInstance().getId()).getRef();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                animais = new ArrayList<>();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    final Animal animal = postSnapshot.getValue(Animal.class);
                    animais.add(animal);
                }
                adapterMeusAnimais.atualiza(animais);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                animais = new ArrayList<>();
                adapterMeusAnimais.atualiza(animais);
                progressDialog.dismiss();
            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_meus_animais, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_meu_perfil:
                startActivity(new Intent(getActivity(), PerfilActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

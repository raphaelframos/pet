package com.powellapps.mymigo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.powellapps.mymigo.fragments.VacinaDialogFragment;
import com.powellapps.mymigo.model.Animal;
import com.powellapps.mymigo.model.Singleton;
import com.powellapps.mymigo.model.Vacina;
import com.powellapps.mymigo.utils.AlertaUtils;
import com.powellapps.mymigo.utils.ConstantsUtils;
import com.powellapps.mymigo.utils.DateUtils;
import com.powellapps.mymigo.utils.GeralUtils;
import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.define.Define;

import java.util.ArrayList;
import java.util.Calendar;


public class NovoAnimalActivity extends AppCompatActivity implements VacinaDialogFragment.VacinaCallback{

    private EditText editTextNome;
    private EditText editTextNascimento;
    private Spinner spinnerTipos;
    private Spinner spinnerRacas;
    private Spinner spinnerSexo;
    private Spinner spinnerPorte;
    private Spinner spinnerCor;
    private Switch switchPedigree;
    private Switch switchFotos;
    private Switch switchVacina;
    private Animal animal;
    private ArrayList<Uri> path;
    private ProgressDialog progressAnimalDialog;
    private ArrayList<Vacina> vacinas;
    ArrayList<String> imagens = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_animal);

        switchFotos = (Switch) findViewById(R.id.switchFotos);
        switchVacina = (Switch) findViewById(R.id.switchVacina);
        switchPedigree = (Switch) findViewById(R.id.switchPedigree);
        editTextNome = (EditText) findViewById(R.id.editTextNomeAnimal);
        editTextNascimento = (EditText) findViewById(R.id.editTextNascimentoAnimal);
        spinnerCor = (Spinner) findViewById(R.id.spinnerCores);
        spinnerPorte = (Spinner) findViewById(R.id.spinnePortes);
        spinnerRacas = (Spinner) findViewById(R.id.spinnerRacas);
        spinnerSexo = (Spinner) findViewById(R.id.spinnerSexos);
        spinnerTipos = (Spinner) findViewById(R.id.spinnerTipos);

        switchFotos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    FishBun.with(NovoAnimalActivity.this)
                            .setMaxCount(3)
                            .startAlbum();
                }
            }
        });

        switchVacina.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    VacinaDialogFragment vacinaDialogFragment = new VacinaDialogFragment();
                    vacinaDialogFragment.setCallback(NovoAnimalActivity.this);
                    vacinaDialogFragment.show(getSupportFragmentManager(), "vacinafragment");
                }else{
                    vacinas = new ArrayList<Vacina>();
                }

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.actionSalvar:
                capturaDados();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void capturaDados() {
        if(animal == null){
            animal = new Animal();
        }

        animal.setNome(editTextNome.getText().toString());
        animal.setSexo((String) spinnerSexo.getSelectedItem());
        animal.setCor((String) spinnerCor.getSelectedItem());
        animal.setRaca((String) spinnerRacas.getSelectedItem());
        animal.setTamanho((String) spinnerPorte.getSelectedItem());
        animal.setTipo((String) spinnerTipos.getSelectedItem());
        animal.setPedigree(switchPedigree.isChecked());
        animal.setNascimento(DateUtils.format(editTextNascimento.getText().toString()));
        animal.setPorte((String) spinnerPorte.getSelectedItem());
        animal.setVacinas(vacinas);
        animal.setIdProprietario(Singleton.getInstance().getId());
        animal.setDataCriacao(Calendar.getInstance().getTimeInMillis());

        progressAnimalDialog = new ProgressDialog(NovoAnimalActivity.this);
        progressAnimalDialog.setMessage("Salvando animal...");
        progressAnimalDialog.setCancelable(false);
        progressAnimalDialog.show();
        String key = salvaAnimal();
        GeralUtils.mostra(key);
        progressAnimalDialog.dismiss();
        salvaImagens(key);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_novo_animal, menu);
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent imageData) {
        super.onActivityResult(requestCode, resultCode, imageData);
        switch (requestCode) {
            case Define.ALBUM_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    path = imageData.getParcelableArrayListExtra(Define.INTENT_PATH);
                    break;
                }
        }
    }

    private void salvaImagens(final String key) {

        final ProgressDialog progressDialog = new ProgressDialog(NovoAnimalActivity.this);
        progressDialog.setMessage("Salvando imagens...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try{
            imagens = new ArrayList<>();
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl(ConstantsUtils.STORAGE_IMAGENS);
            if(path.size() > 0) {
                for (int i = 0; i < path.size(); i++) {
                    Uri file = path.get(i);
                    StorageReference riversRef = storageRef.child(ConstantsUtils.REF_IMAGENS)
                            .child(Singleton.getInstance().getId())
                            .child(key)
                            .child(file.getLastPathSegment());
                    UploadTask uploadTask = riversRef.putFile(file);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception exception) {
                            exception.printStackTrace();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            GeralUtils.mostra(downloadUrl.toString());
                            imagens.add(downloadUrl.toString());
                        }
                    }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(Task<UploadTask.TaskSnapshot> task) {
                            if (imagens.size() >= 0) {
                                animal.setImagens(imagens);
                                atualizaAnimal(key);
                            }
                            fechaProgress(progressDialog);
                        }
                    })
                    ;
                }
            }else{
                fechaProgress(progressDialog);
            }
        }catch (Exception e){
            e.printStackTrace();
            fechaProgress(progressDialog);
        }

    }

    private String salvaAnimal() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ConstantsUtils.REF_ANIMAIS);
        GeralUtils.mostra(Singleton.getInstance().getId());
        String key = myRef
                .push().getKey();

        myRef.child(key).setValue(animal);
        return key;
    }

    private void atualizaAnimal(String key) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ConstantsUtils.REF_ANIMAIS);
        myRef.child(key).setValue(animal);
    }


    private void fechaProgress(ProgressDialog progressDialog){
        try{
            progressDialog.dismiss();
            AlertaUtils.alertaComUmaOpcao(NovoAnimalActivity.this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }, "Animal adicionado com sucesso.");
        }catch (Exception e){}
    }

    @Override
    public void adiciona(ArrayList<Vacina> vacinas) {
        this.vacinas = vacinas;
    }
}
